package com.atguigu.gulimall.thirdparty.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.common.utils.BinaryUtil;
import com.aliyun.oss.model.MatchMode;
import com.aliyun.oss.model.PolicyConditions;
import com.atguigu.common.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: cccyl
 * @Date: 2021/04/08/14:11
 * @Description:
 */
@RestController
@RequestMapping("/thirdparty")
public class OssController {

    @Autowired
    OSS oss;

    @Value("${spring.cloud.alicloud.access-key}")
    String accessId;

    @Value("${spring.cloud.alicloud.oss.bucket}")
    String bucket;

    @Value("${spring.cloud.alicloud.oss.endpoint}")
    String endpoint;

    @GetMapping("/oss/policy")
    public R policy() {

        Map<String,String> resultMap = new HashMap<>();

        String host = "https://" + bucket + "." + endpoint;

        //用户上传时指定的前缀
        String format = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

        try {
            //签名有效时间
            long expiretime = 30;
            long expireEndTime = System.currentTimeMillis() + expiretime * 1000;

            Date expiration = new Date(expireEndTime);

            PolicyConditions policyConditions = new PolicyConditions();
            policyConditions.addConditionItem(PolicyConditions.COND_CONTENT_LENGTH_RANGE, 0, 1048576000);
            policyConditions.addConditionItem(MatchMode.StartWith, PolicyConditions.COND_KEY, format);

            String policy = oss.generatePostPolicy(expiration, policyConditions);
            byte[] policyBytes = policy.getBytes(StandardCharsets.UTF_8);
            String base64String = BinaryUtil.toBase64String(policyBytes);

            //签名
            String postSignature = oss.calculatePostSignature(policy);

            resultMap= new LinkedHashMap<String, String>();
            resultMap.put("accessid", accessId);
            resultMap.put("policy", base64String);
            resultMap.put("signature", postSignature);
            resultMap.put("dir", format);
            resultMap.put("host", host);
            resultMap.put("expire", String.valueOf(expireEndTime / 1000));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            oss.shutdown();
        }

        return R.ok().put("data",resultMap);
    }
}
