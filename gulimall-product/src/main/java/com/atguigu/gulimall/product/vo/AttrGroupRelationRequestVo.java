package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author cyl
 * @date 2021/11/15/21:10
 * @description
 */
@Data
public class AttrGroupRelationRequestVo {

    private Long attrId;

    private Long attrGroupId;
}
