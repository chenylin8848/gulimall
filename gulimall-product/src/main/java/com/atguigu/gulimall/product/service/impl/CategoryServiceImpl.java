package com.atguigu.gulimall.product.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.web.Catelog2Vo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Service("categoryService")
@Slf4j
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RedissonClient redisson;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<CategoryEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        //1.找出所有分类
        List<CategoryEntity> entities = baseMapper.selectList(null);

        //2.组装成父子关系的树形结构
        //2.1找出所有的一级分类
        List<CategoryEntity> levelOneCategory = entities.stream()
                .filter(e -> e.getCatLevel() == 1)
                .map(menu -> {
                    menu.setChildren(getChildren(menu, entities));
                    return menu;
                }).sorted((menu1, menu2) -> {
                    return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
                }).collect(Collectors.toList());

        return levelOneCategory;
    }

    @Override
    public void removeMenusByIds(List<Long> asList) {
        //TODO 1.检查当前菜单是否被其他菜单引用

        baseMapper.deleteBatchIds(asList);
    }

    /**
     * 查询三级分类id的完整路径
     *
     * @param catelogId
     * @return
     */
    @Override
    public Long[] findCategoryPath(Long catelogId) {
        List<Long> paths = new ArrayList<>();

        List<Long> parentPath = findParentPath(catelogId, paths);

        Collections.reverse(parentPath);

        return parentPath.toArray(new Long[parentPath.size()]);
    }

    @Override
    public void updateDetail(CategoryEntity category) {
        updateById(category);

        if (StringUtils.isNoneBlank(category.getName())) {
            categoryBrandRelationService.updateCategory(category.getCatId(), category.getName());

            //todo 更新其他关联
        }
    }

    /**
     * 每一个要缓存的数据要指定要放到哪个名字的缓存【缓存的分区（按照业务类型分）】
     * @Cacheable 代表当前方法的结果需要缓存，如果缓存中有，方法不用调用，如果缓存中没有，就会调用方法，最后将方法的结果放入缓存
     *      缓存的值，默认使用Java序列化机制，将序列化后的值存到redis,过期时间默认是-1
     *      自定义key：key属性
     *      缓存时间:配置文件中修改ttl
     *      将保存的数据修改为json格式:
     *
     * @return
     */
    //
//    @Cacheable(value = {"category"},key = "'getLevelOneCategory'")
    @Cacheable(value = {"category"},key = "#root.methodName", sync = true)
    @Override
    public List<CategoryEntity> getLevelOneCategory() {
        log.info("getLevelOneCategory...............");
        LambdaQueryWrapper<CategoryEntity> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(CategoryEntity::getParentCid, 0);
        List<CategoryEntity> list = list(queryWrapper);
        return list;
//        return null;
    }

    /**
     * 缓存里面的数据如何和数据库保持一致
     * 缓存一致性
     *  1)、双写模式
     *  2)、失效模式
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCatelogJson2() {
//        return getCatelogJsonWithLocalLock();
//        return getCatelogJsonWithRedisLock();
        return getCatelogJsonWithRedissonLock();
    }

    @Cacheable(value = {"category"},key = "#root.methodName")
    @Override
    public Map<String, List<Catelog2Vo>> getCatelogJson() {
        /**
         * 1.优化，一次查出所有数据
         */
        List<CategoryEntity> list = list();

        //1.查出所有的一级分类
        Map<String, List<Catelog2Vo>> map = getCatelogByParentId(list, 0l).stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //查到这个一级分类的二级分类
            List<CategoryEntity> catelog2List = getCatelogByParentId(list, v.getCatId());
            List<Catelog2Vo> catelog2VoList = null;
            if (catelog2List != null) {

                catelog2VoList = catelog2List.stream().map(item -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo();

                    catelog2Vo.setCatalog1Id(v.getCatId().toString());
                    catelog2Vo.setId(item.getCatId().toString());
                    catelog2Vo.setName(item.getName());

                    List<CategoryEntity> catelog3List = getCatelogByParentId(list, item.getCatId());

                    List<Catelog2Vo.Catelog3Vo> catelog3VoList = null;
                    if (catelog3List != null) {
                        catelog3VoList = catelog3List.stream().map(entity -> {
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo();
                            catelog3Vo.setName(entity.getName());
                            catelog3Vo.setCatalog2Id(item.getCatId().toString());
                            catelog3Vo.setId(entity.getCatId().toString());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                    }

                    catelog2Vo.setCatalog3List(catelog3VoList);

                    return catelog2Vo;


                }).collect(Collectors.toList());
            }

            return catelog2VoList;
        }));

        //将查出的对象转json,存进缓存
        String jsonString = JSONObject.toJSONString(map);
        stringRedisTemplate.opsForValue().set("catelogJson", jsonString);

        return map;
    }

    public Map<String, List<Catelog2Vo>> getCatelogJsonWithRedissonLock() {

        //1.锁的名字，锁的粒度，越细越快
        RLock redissonLock = redisson.getLock("CatelogJson-lock");

        redissonLock.lock();

        //加锁成功
        Map<String, List<Catelog2Vo>> stringListMap;
        try {
            stringListMap = getStringListMap();
        } finally {
            redissonLock.unlock();
        }

        return stringListMap;

    }

    /**
     * 使用redis实现分布式锁
     * 核心：加锁/解锁都必须是原子操作
     *
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCatelogJsonWithRedisLock() {


        //加锁
        String token = UUID.randomUUID().toString();
        Boolean lock = stringRedisTemplate.opsForValue().setIfAbsent("lock", token, 30, TimeUnit.SECONDS);
        if (lock) {
            //加锁成功
            Map<String, List<Catelog2Vo>> stringListMap;
            try {
                stringListMap = getStringListMap();

            } finally {
                String lockValue = stringRedisTemplate.opsForValue().get("lock");
                if (lockValue.equals(token)) {
                    //拿到的是自己加的锁，进行解锁
                    // get和delete原子操作
                    String script = "if redis.call('get', KEYS[1]) == ARGV[1] " +
                            "then return redis.call('del', KEYS[1]) else return 0 end";
                    stringRedisTemplate.execute(
                            new DefaultRedisScript<Long>(script, Long.class), // 脚本和返回类型
                            Arrays.asList("lock"), // 参数
                            lockValue); // 参数值，锁的值
                }

            }

            return stringListMap;
        } else {
            //加锁失败
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 睡眠0.1s后，重新调用 //自旋
            return getCatelogJsonWithRedisLock();
        }
    }

    private Map<String, List<Catelog2Vo>> getStringListMap() {
        ValueOperations<String, String> operations = stringRedisTemplate.opsForValue();

        String catelogJson = operations.get("catelogJson");

        if (StringUtils.isNotBlank(catelogJson)) {
            Map<String, List<Catelog2Vo>> map = JSONObject.parseObject(catelogJson, new TypeReference<Map<String, List<Catelog2Vo>>>() {
            });
            return map;
        } else {
            //缓存中没有
            log.info("缓存中没有，开始查询数据库....");
            Map<String, List<Catelog2Vo>> catelogJsonFromDb = getCatelogJsonFromDb();

            return catelogJsonFromDb;
        }
    }

    /**
     * 本地锁
     *
     * @return
     */
    public Map<String, List<Catelog2Vo>> getCatelogJsonWithLocalLock() {
        synchronized (this) {
            //this 指当前对象，springboot容器中的对象都是单例的
            return getStringListMap();
        }
    }

    public Map<String, List<Catelog2Vo>> getCatelogJsonFromDb() {

        /**
         * 1.优化，一次查出所有数据
         */
        List<CategoryEntity> list = list();

        //1.查出所有的一级分类
        Map<String, List<Catelog2Vo>> map = getCatelogByParentId(list, 0l).stream().collect(Collectors.toMap(k -> k.getCatId().toString(), v -> {
            //查到这个一级分类的二级分类
            List<CategoryEntity> catelog2List = getCatelogByParentId(list, v.getParentCid());
            List<Catelog2Vo> catelog2VoList = null;
            if (catelog2List != null) {

                catelog2VoList = catelog2List.stream().map(item -> {
                    Catelog2Vo catelog2Vo = new Catelog2Vo();

                    catelog2Vo.setCatalog1Id(v.getCatId().toString());
                    catelog2Vo.setId(item.getCatId().toString());
                    catelog2Vo.setName(item.getName());

                    List<CategoryEntity> catelog3List = getCatelogByParentId(list, item.getParentCid());

                    List<Catelog2Vo.Catelog3Vo> catelog3VoList = null;
                    if (catelog3List != null) {
                        catelog3VoList = catelog3List.stream().map(entity -> {
                            Catelog2Vo.Catelog3Vo catelog3Vo = new Catelog2Vo.Catelog3Vo();
                            catelog3Vo.setName(entity.getName());
                            catelog3Vo.setCatalog2Id(item.getCatId().toString());
                            catelog3Vo.setId(entity.getCatId().toString());
                            return catelog3Vo;
                        }).collect(Collectors.toList());
                    }

                    catelog2Vo.setCatalog3List(catelog3VoList);

                    return catelog2Vo;


                }).collect(Collectors.toList());
            }

            return catelog2VoList;
        }));

        //将查出的对象转json,存进缓存
        String jsonString = JSONObject.toJSONString(map);
        stringRedisTemplate.opsForValue().set("catelogJson", jsonString);

        return map;
    }

    public List<CategoryEntity> getCatelogByParentId(List<CategoryEntity> list, Long parentId) {
        return list.stream().filter(item -> {
            return item.getParentCid() == parentId;
        }).collect(Collectors.toList());
    }

    public List<Long> findParentPath(Long catelogId, List<Long> paths) {
        paths.add(catelogId);

        CategoryEntity byId = this.getById(catelogId);

        if (byId.getParentCid() != 0) {
            findParentPath(byId.getParentCid(), paths);
        }

        return paths;
    }

    /**
     * 递归查找所有分类的子分类
     *
     * @param root
     * @param all
     * @return
     */
    public List<CategoryEntity> getChildren(CategoryEntity root, List<CategoryEntity> all) {
        List<CategoryEntity> childrenCollection = all.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid().equals(root.getCatId());
        }).map(categoryEntity -> {
            //递归找到子分类
            categoryEntity.setChildren(getChildren(categoryEntity, all));
            return categoryEntity;
        }).sorted((menu1, menu2) -> {
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());

        return childrenCollection;
    }

}