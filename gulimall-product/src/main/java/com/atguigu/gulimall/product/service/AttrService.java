package com.atguigu.gulimall.product.service;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.ProductAttrValueEntity;
import com.atguigu.gulimall.product.vo.AttrRequestVo;
import com.atguigu.gulimall.product.vo.AttrResponseVo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author chenyuanlin
 * @email chenyuanlin@gmail.com
 * @date 2020-11-15 23:45:32
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttrVo(AttrRequestVo attr);

    PageUtils queryBaseAttrListPage(Map<String, Object> params, Long catelogId, String attrType);

    AttrResponseVo getAttrInfo(Long attrId);

    void updateAttr(AttrRequestVo attrRequestVo);

    List<AttrEntity> getAttrRelatioin(Long attrgroupId);

    /**
     * 获取当前分组没有关联的其他属性
     * @param attrgroupId
     * @param params
     * @return
     */
    PageUtils getAttrNoRelatioin(Long attrgroupId, Map<String, Object> params);

    void updateSpuAttr(Long spuId, List<ProductAttrValueEntity> list);
}

