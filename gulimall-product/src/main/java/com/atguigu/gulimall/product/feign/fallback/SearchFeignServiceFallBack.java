package com.atguigu.gulimall.product.feign.fallback;

import com.atguigu.common.to.es.SkuEsModel;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.feign.SearchFeignService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author cyl
 * @date 2021/11/17/22:22
 * @description
 */
@Service
public class SearchFeignServiceFallBack implements SearchFeignService {

    @Override
    public R productUp(List<SkuEsModel> skuEsModelList) {
        return null;
    }
}
