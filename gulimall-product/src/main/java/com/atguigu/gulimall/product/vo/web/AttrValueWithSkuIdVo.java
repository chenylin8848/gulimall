package com.atguigu.gulimall.product.vo.web;

import lombok.Data;

/**
 * @author cyl
 * @date 2021/12/14/21:00
 * @description
 */
@Data
public class AttrValueWithSkuIdVo {

    private String attrValue;

    private String skuIds;
}
