package com.atguigu.gulimall.product.vo.web;

import lombok.Data;

/**
 * @author cyl
 * @date 2021/12/13/21:48
 * @description
 */
@Data
public class SpuItemBaseAttrVo {
    private String attrName;

    private String attrValue;
}
