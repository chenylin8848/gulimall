package com.atguigu.gulimall.product.web;

import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RCountDownLatch;
import org.redisson.api.RLock;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RSemaphore;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author cyl
 * @date 2021/12/04/10:23
 * @description
 */
@Controller
@Slf4j
public class RedissonContrller {

    @Autowired
    private RedissonClient redisson;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    @ResponseBody
    @GetMapping("/hello")
    public String hello() {

        //1、获取一把锁，只要锁的名字一样，就是一把锁
        RLock lock = redisson.getLock("my-lock");

        //2、加锁
//        lock.lock(); //阻塞式等待 默认加锁时间是30s
        //1)、锁的自动续期，如果业务超长，运行期间自动给锁续上新的30s，不用担心业务时间长，锁过期自动被删掉
        //2)、加锁的业务只要运行完成，就不会给当前锁续期，即使不手动解锁，锁也会默认在30s以后自动删除

        lock.lock(10, TimeUnit.SECONDS); //10s自动解锁，自动解锁时间一定要大于业务的执行时间.在锁的时间到了之后，不对自动续期
        //1.如果我们传递了锁的超时时间，就发送给redis执行脚本，进行占锁，默认超时就是我们指定的时间
        //2.如果我们未指定锁的超时时间，就使用【LockWatchdogTimeout --30】看门狗的默认时间。
        // 只要占锁成功，就会启动一个定时任务【重新给锁设hi过期时间，默认时间就是看门狗的默认时间】
        // 【internalLockLeaseTime / 3L】定时任务触发

        //最佳实战
        //1.lock.lock(10,TimeUnit.SECONDS);省掉了整个续期操作

        try {
            log.info("加锁成功.........." + Thread.currentThread().getId());
            Thread.sleep(30000);
        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            //3、解锁
            //假设解锁代码没有运行，会不会出现死锁
            log.info("解锁成功..........." + Thread.currentThread().getId());
            lock.unlock();
        }
        return "hello";
    }

    /**
     * 读 + 读：相当于无锁；并发读，只会在redis中记录好，所有的当前锁，他们会同时加锁成功
     * 写 + 读：等待写锁释放
     * 写 + 写：阻塞状态
     * 读 + 写：等待读锁释放
     * 只要有写的存在，都必须等待
     * @return
     */
    @ResponseBody
    @GetMapping("/write")
    public String writeValue() {
        String s = "";

        RReadWriteLock readWriteLock = redisson.getReadWriteLock("rw-lock");
        RLock rLock = readWriteLock.writeLock();
        try {
            //1.改数据在写锁
            rLock.lock();
            s = UUID.randomUUID().toString();
//            Thread.sleep(30000);
            stringRedisTemplate.opsForValue().set("writeValue", s);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rLock.unlock();
        }
        return s;
    }

    @ResponseBody
    @GetMapping("/read")
    public String readValue() {
        String s = "";
        RReadWriteLock readWriteLock = redisson.getReadWriteLock("rw-lock");
        //加读锁
        RLock rLock = readWriteLock.readLock();
        try {
            rLock.lock();
            Thread.sleep(30000);
            s = stringRedisTemplate.opsForValue().get("writeValue");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            rLock.unlock();
        }
        return s;
    }

    /**
     * 车库停车：3个车位
     * 信号量可以用来做分布式限流
     */
    @ResponseBody
    @GetMapping("/park")
    public String park() throws InterruptedException {
        RSemaphore park = redisson.getSemaphore("park");

        park.acquire();//获取一个信号，获取一个车位

        return "ok!";
    }

    /**
     * 车开走
     */
    @ResponseBody
    @GetMapping("/go")
    public String go() {
        RSemaphore park = redisson.getSemaphore("park");
        park.release(); //释放一个车位
        return "ok!";
    }

    /**
     * 放假：锁门
     * 5个班的人都走了，才锁门
     */
    @ResponseBody
    @GetMapping("/lockDoor")
    public String lockDoor() throws InterruptedException {

        RCountDownLatch door = redisson.getCountDownLatch("door");
        door.trySetCount(5);
        door.await(); //等待闭锁都完成

        return "放假了...";
    }

    @ResponseBody
    @GetMapping("/goHome/{id}")
    public String goHome(@PathVariable("id") Long id) {
        RCountDownLatch dock = redisson.getCountDownLatch("door");

        dock.countDown();//计数减一

        return id + "回家了...";
    }
}
