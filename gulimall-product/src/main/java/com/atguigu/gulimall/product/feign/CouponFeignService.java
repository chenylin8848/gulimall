package com.atguigu.gulimall.product.feign;

import com.atguigu.common.to.SkuReductionTo;
import com.atguigu.common.to.SpuBoundsTo;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.feign.fallback.CouponFeignServiceFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author cyl
 * @date 2021/11/17/22:20
 * @description
 */
@FeignClient(name = "gulimall-coupon", fallback = CouponFeignServiceFallBack.class)
public interface CouponFeignService {

    /**
     * 保存
     */
    @PostMapping("/coupon/spubounds/savespubounds")
    public R saveSpuBounds(@RequestBody SpuBoundsTo spuBoundsTo);

    @PostMapping("/coupon/skufullreduction/saveSkuReduction")
    public R saveSkuReduction(SkuReductionTo skuReductionTo);
}
