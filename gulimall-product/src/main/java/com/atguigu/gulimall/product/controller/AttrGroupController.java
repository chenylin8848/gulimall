package com.atguigu.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.service.AttrAttrgroupRelationService;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.service.AttrGroupService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;



/**
 * 属性分组
 *
 * @author chenyuanlin
 * @email chenyuanlin@gmail.com
 * @date 2020-11-16 00:12:56
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    /**
     * 三级分类
     */
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    /**
     * 属性
     */
    @Autowired
    private AttrService attrService;

    /**
     * 获取分类下所有分组&关联属性
     * @return
     */
    @GetMapping("/{catelogId}/withattr")
    public R getAttrGroupWithAttrs(@PathVariable("catelogId") Long catelogId) {

        List<AttrGroupWithAttrsVo> list = attrGroupService.getAttrGroupWithAttrsByCatelogId(catelogId);

        return R.ok().put("data",list);
    }

    /**
     *  /product/attrgroup/{attrgroupId}/attr/relation
     * 获取属性分组的关联的所有属性
     * @param attrgroupId
     * @return
     */
    @GetMapping("/{attrgroupId}/attr/relation")
    public R getAttrRelatioin(@PathVariable("attrgroupId") Long attrgroupId) {
        List<AttrEntity> entityList = attrService.getAttrRelatioin(attrgroupId);

        return R.ok().put("data", entityList);
    }

    /**
     *  /product/attrgroup/{attrgroupId}/noattr/relation
     * 获取属性分组没有关联的其他属性
     * @param attrgroupId
     * @return
     */
    @GetMapping("/{attrgroupId}/noattr/relation")
    public R getAttrNoRelatioin(@PathVariable("attrgroupId") Long attrgroupId,
                                @RequestParam Map<String, Object> params) {
        PageUtils page = attrService.getAttrNoRelatioin(attrgroupId,params);

        return R.ok().put("page", page);
    }

    /**
     *  /product/attrgroup/attr/relation
     * 获取属性分组没有关联的其他属性
     * @param attrgroupId
     * @return
     */
    @PostMapping("/attr/relation")
    public R addAttrRelation(@RequestBody AttrAttrgroupRelationEntity[] vos) {
        attrAttrgroupRelationService.addAttrRelation(vos);

        return R.ok();
    }

    /**
     * /product/attrgroup/attr/relation/delete
     * 删除属性与分组的关联关系
     */
    @PostMapping("/attr/relation/delete")
    public R deletetAttrRelation(@RequestBody AttrAttrgroupRelationEntity[] vos) {
        attrAttrgroupRelationService.deletetAttrRelation(vos);

        return R.ok();
    }

    /**
     * 列表
     */
    @RequestMapping("/list/{catelogId}")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable("catelogId") Long catelogId){
//        PageUtils page = attrGroupService.queryPage(params);
        PageUtils page = attrGroupService.queryPage(params,catelogId);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId){
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

        Long catelogId = attrGroup.getCatelogId();

        Long[] categoryIds = categoryService.findCategoryPath(catelogId);

        attrGroup.setCatelogIds(categoryIds);

        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.save(attrGroup);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrGroupEntity attrGroup){
		attrGroupService.updateById(attrGroup);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] attrGroupIds){
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));

        return R.ok();
    }

}
