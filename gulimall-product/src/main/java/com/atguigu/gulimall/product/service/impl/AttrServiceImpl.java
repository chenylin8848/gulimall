package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.constant.ProductConstant;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.AttrDao;
import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.entity.ProductAttrValueEntity;
import com.atguigu.gulimall.product.service.AttrAttrgroupRelationService;
import com.atguigu.gulimall.product.service.AttrGroupService;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrRequestVo;
import com.atguigu.gulimall.product.vo.AttrResponseVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    private AttrAttrgroupRelationService attrAttrgroupRelationService;

    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public void saveAttrVo(AttrRequestVo attr) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attr, attrEntity);
        //保存基本数据
        save(attrEntity);

        //保存关联关系
        AttrAttrgroupRelationEntity relation = new AttrAttrgroupRelationEntity();

        if (attr.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode() &&
            attr.getAttrGroupId() != null) {
            //基本属性
            relation.setAttrId(attrEntity.getAttrId());
            relation.setAttrGroupId(attr.getAttrGroupId());
            attrAttrgroupRelationService.save(relation);
        }
    }

    @Override
    public PageUtils queryBaseAttrListPage(Map<String, Object> params, Long catelogId, String attrType) {
        QueryWrapper<AttrEntity> queryWrapper = new QueryWrapper<>();

        queryWrapper.eq("base".equalsIgnoreCase(attrType), "attr_type", ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode())
                .eq("sale".equalsIgnoreCase(attrType), "attr_type", ProductConstant.AttrEnum.ATTR_TYPE_SALE.getCode());

        if (catelogId != 0) {
            queryWrapper.eq("catelog_id", catelogId);
        }

        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            queryWrapper.and((obj) -> {
                obj.eq("attr_id", key).or().like("attr_name", key);
            });
        }
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                queryWrapper
        );

        PageUtils pageUtils = new PageUtils(page);

        List<AttrEntity> list = page.getRecords();

        List<AttrResponseVo> attrResponseVos = list.stream().map(attrEntity -> {

            AttrResponseVo attrResponseVo = new AttrResponseVo();
            BeanUtils.copyProperties(attrEntity, attrResponseVo);

            if ("base".equalsIgnoreCase(attrType)) {

                AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationService.getOne(
                        new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                                .eq(AttrAttrgroupRelationEntity::getAttrId, attrEntity.getAttrId()));

                if (attrAttrgroupRelationEntity != null && attrAttrgroupRelationEntity.getAttrGroupId() != null) {
                    Long attrGroupId = attrAttrgroupRelationEntity.getAttrGroupId();
                    AttrGroupEntity attrGroupEntity = attrGroupService.getById(attrGroupId);
                    attrResponseVo.setGroupName(attrGroupEntity.getAttrGroupName());

                }
            }

            CategoryEntity categoryEntity = categoryService.getById(attrEntity.getCatelogId());

            if (categoryEntity != null) {

                attrResponseVo.setCatelogName(categoryEntity.getName());
            }

            return attrResponseVo;
        }).collect(Collectors.toList());

        pageUtils.setList(attrResponseVos);
        return pageUtils;

    }

    @Cacheable(value = "attr", key = "'attrinfo:' + #root.args[0]")
    @Override
    public AttrResponseVo getAttrInfo(Long attrId) {
        AttrResponseVo attrResponseVo = new AttrResponseVo();

        AttrEntity attrEntity = getById(attrId);
        BeanUtils.copyProperties(attrEntity, attrResponseVo);

        if (attrEntity.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()) {

            //1.设置属性分组信息
            AttrAttrgroupRelationEntity attrAttrgroupRelation = attrAttrgroupRelationService.getOne(
                    new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                            .eq(AttrAttrgroupRelationEntity::getAttrId, attrEntity.getAttrId()));

            if (attrAttrgroupRelation != null) {

                attrResponseVo.setAttrGroupId(attrAttrgroupRelation.getAttrGroupId());

                AttrGroupEntity attrGroupEntity =
                        attrGroupService.getById(attrAttrgroupRelation.getAttrGroupId());

                if (attrGroupEntity != null) {
                    attrResponseVo.setGroupName(attrGroupEntity.getAttrGroupName());
                }
            }
        }

        //2.设置分类信息
        Long catelogId = attrEntity.getCatelogId();
        CategoryEntity categoryEntity = categoryService.getById(catelogId);

        if (categoryEntity != null) {
            attrResponseVo.setCatelogName(categoryEntity.getName());
        }

        Long[] categoryPath = categoryService.findCategoryPath(catelogId);
        attrResponseVo.setCatelogPath(categoryPath);

        return attrResponseVo;
    }

    @Override
    public void updateAttr(AttrRequestVo attrRequestVo) {
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attrRequestVo, attrEntity);
        updateById(attrEntity);

        if (attrRequestVo.getAttrType() == ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode()) {

            // 1.修改分组关联
            int count = attrAttrgroupRelationService.count(new LambdaQueryWrapper<AttrAttrgroupRelationEntity>()
                    .eq(AttrAttrgroupRelationEntity::getAttrId, attrRequestVo.getAttrId()));

            AttrAttrgroupRelationEntity relation = new AttrAttrgroupRelationEntity();
            relation.setAttrGroupId(attrRequestVo.getAttrGroupId());
            relation.setAttrId(attrRequestVo.getAttrId());
            if (count > 0) {
                attrAttrgroupRelationService.update(relation, new LambdaUpdateWrapper<AttrAttrgroupRelationEntity>()
                        .eq(AttrAttrgroupRelationEntity::getAttrId, attrRequestVo.getAttrId()));
            } else {
                attrAttrgroupRelationService.save(relation);
            }
        }
    }

    /**
     * 根据分组查找所有关联的基本属性
     *
     * @param attrgroupId
     * @return
     */
    @Override
    public List<AttrEntity> getAttrRelatioin(Long attrgroupId) {
        LambdaQueryWrapper<AttrAttrgroupRelationEntity> queryWrapper = Wrappers.lambdaQuery();
        queryWrapper.eq(AttrAttrgroupRelationEntity::getAttrGroupId, attrgroupId);
        List<AttrAttrgroupRelationEntity> relationEntities = attrAttrgroupRelationService.list(queryWrapper);

        List<Long> longList = relationEntities.stream().map((attr) -> {
            return attr.getAttrId();
        }).collect(Collectors.toList());

        if (longList == null || longList.size() == 0) {
            return null;
        }

        List<AttrEntity> attrEntities = this.listByIds(longList);

        return attrEntities;
    }

    @Override
    public PageUtils getAttrNoRelatioin(Long attrgroupId, Map<String, Object> params) {

        //1.当前分组只能关联自己所属分类里面的属性
        AttrGroupEntity attrGroupEntity = attrGroupService.getById(attrgroupId);

        Long catelogId = attrGroupEntity.getCatelogId();

        //2.当前分组只能关联别的分组没有引用的属性
        //2.1找到当前分类下的所有分组
        LambdaQueryWrapper<AttrGroupEntity> queryWrapper = Wrappers.lambdaQuery();

        queryWrapper.eq(AttrGroupEntity::getCatelogId, catelogId);
        List<AttrGroupEntity> attrGroupEntities = attrGroupService.list(queryWrapper);

        List<Long> gropuIds = attrGroupEntities.stream().map(item -> {
            return item.getAttrGroupId();
        }).collect(Collectors.toList());

        //2.2这些分组关联的属性
        LambdaQueryWrapper<AttrAttrgroupRelationEntity> relationWrapper = Wrappers.lambdaQuery();
        relationWrapper.in(AttrAttrgroupRelationEntity::getAttrGroupId, gropuIds);
        List<AttrAttrgroupRelationEntity> relationEntities = attrAttrgroupRelationService.list(relationWrapper);

        List<Long> attrIds = relationEntities.stream().map(item -> {
            return item.getAttrId();
        }).collect(Collectors.toList());

        //2.3从当前分类的所有属性移除这些属性
        LambdaQueryWrapper<AttrEntity> attrWrapper = Wrappers.lambdaQuery();
        attrWrapper.eq(AttrEntity::getCatelogId, catelogId);
        attrWrapper.eq(AttrEntity::getAttrType,ProductConstant.AttrEnum.ATTR_TYPE_BASE.getCode());
        if (attrIds != null && attrIds.size() > 0) {

            attrWrapper.notIn(AttrEntity::getAttrId, attrIds);
        }
        String key = (String) params.get("key");
        if (StringUtils.isNotBlank(key)) {
            attrWrapper.eq(AttrEntity::getAttrId, key).or().like(AttrEntity::getAttrName, key);
        }

        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), attrWrapper);

        return new PageUtils(page);
    }

    @Override
    public void updateSpuAttr(Long spuId, List<ProductAttrValueEntity> list) {

    }

}