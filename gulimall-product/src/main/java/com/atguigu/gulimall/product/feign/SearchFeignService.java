package com.atguigu.gulimall.product.feign;

import com.atguigu.common.to.es.SkuEsModel;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.feign.fallback.SearchFeignServiceFallBack;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author cyl
 * @date 2021/11/28/10:25
 * @description
 */
@FeignClient(value = "gulimall-search",fallback = SearchFeignServiceFallBack.class)
public interface SearchFeignService {

    @PostMapping("/search/product/save")
    public R productUp(@RequestBody List<SkuEsModel> skuEsModelList);
}
