package com.atguigu.gulimall.product.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author cyl
 * @date 2021/12/04/8:15
 * @description
 */
@Configuration
public class MyRedissonConfig {

    /**
     * 所有对redisson的使用都是通过对RedissonClient的使用
     * @return
     */
    @Bean(destroyMethod = "shutdown")
    public RedissonClient redisson() {
        Config config = new Config();
        config.useSingleServer().setAddress("redis://47.106.166.2:6379").setPassword("#e!pTa/qh6-h")
                .setConnectionMinimumIdleSize(5); // Unable to init enough connections amount! Only 22 of 24 were initialized

        return Redisson.create(config);
    }
}
