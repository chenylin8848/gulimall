package com.atguigu.gulimall.product.feign.fallback;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.feign.WareFeignService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author cyl
 * @date 2021/11/17/22:22
 * @description
 */
@Service
public class WareFeignServiceFallBack implements WareFeignService {

    @Override
    public R getSkuHasStock(List<Long> skuIds) {
        return null;
    }
}
