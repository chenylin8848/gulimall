package com.atguigu.gulimall.product.feign.fallback;

import com.atguigu.common.to.SkuReductionTo;
import com.atguigu.common.to.SpuBoundsTo;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.feign.CouponFeignService;
import org.springframework.stereotype.Service;

/**
 * @author cyl
 * @date 2021/11/17/22:22
 * @description
 */
@Service
public class CouponFeignServiceFallBack implements CouponFeignService {
    @Override
    public R saveSpuBounds(SpuBoundsTo spuBoundsTo) {
        return null;
    }

    @Override
    public R saveSkuReduction(SkuReductionTo skuReductionTo) {
        return null;
    }
}
