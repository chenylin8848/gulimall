package com.atguigu.gulimall.product.vo.web;

import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @date 2021/12/13/21:48
 * @description
 */
@Data
public class SkuItemSaleAttrVo {
    private Long attrId;

    private String attrName;

    private List<AttrValueWithSkuIdVo> attrValues;
}
