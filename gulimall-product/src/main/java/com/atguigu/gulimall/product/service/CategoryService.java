package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.vo.web.Catelog2Vo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.CategoryEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author chenyuanlin
 * @email chenyuanlin@gmail.com
 * @date 2020-11-15 23:45:32
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<CategoryEntity> listWithTree();

    void removeMenusByIds(List<Long> asList);

    /**
     * 查询三级分类id的完整路径
     * @param catelogId
     * @return [父、子、孙]
     */
    Long[] findCategoryPath(Long catelogId);

    /**
     * 级联更新所有关联的数据
     * @param category
     */
    void updateDetail(CategoryEntity category);

    /**
     * 获取所有的一级分类
     * @return
     */
    List<CategoryEntity> getLevelOneCategory();

    Map<String, List<Catelog2Vo>> getCatelogJson();
}

