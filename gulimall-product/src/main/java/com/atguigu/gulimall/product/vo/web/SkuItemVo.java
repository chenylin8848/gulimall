package com.atguigu.gulimall.product.vo.web;

import com.atguigu.gulimall.product.entity.SkuImagesEntity;
import com.atguigu.gulimall.product.entity.SkuInfoEntity;
import com.atguigu.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @date 2021/12/13/20:59
 * @description
 */
@Data
public class SkuItemVo {
    //1.sku基本信息获取--pms_sku_info
    private SkuInfoEntity info;

    //2.sku的图片信息--pms_sku_images
    private List<SkuImagesEntity> images;

    //3.获取spu的销售属性组合
    private List<SkuItemSaleAttrVo> saleAttr;

    //4.获取spu的介绍
    private SpuInfoDescEntity desc;

    //5.获取spu的规格参数信息
    private List<SpuItemAttrGroupVo> groupAttr;

    private Boolean hasStock = true;

    private Object seckillSkuVo;

}
