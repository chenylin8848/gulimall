package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.BrandDao;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;


@Service("brandService")
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        //1、获取key
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> queryWrapper = new QueryWrapper<>();
        //如果传过来的数据不是空的，就进行多参数查询
        if (StringUtils.isNoneBlank(key)) {
            queryWrapper.eq("brand_id",key).or().like("name",key);
        }

        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public void updateDetail(BrandEntity brandEntity) {
        //保证冗余字段的数据一直，品牌-分类表中存有品牌的名字，但品牌名字改变时，品牌-分类表也需要更改
        updateById(brandEntity);

        if (StringUtils.isNoneBlank(brandEntity.getName())) {
            //同步更新其他表中关联的数据
            categoryBrandRelationService.updateBrand(brandEntity.getBrandId(),brandEntity.getName());

            //todo 更新其他关联
        }
    }

    @Override
    public List<BrandEntity> getByIds(List<Long> brandIds) {
        LambdaQueryWrapper<BrandEntity> query = Wrappers.lambdaQuery();
        query.in(BrandEntity::getBrandId,brandIds);
        return list(query);
    }

}