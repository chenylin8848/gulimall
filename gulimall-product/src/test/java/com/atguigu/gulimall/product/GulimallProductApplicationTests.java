package com.atguigu.gulimall.product;

import com.atguigu.gulimall.product.dao.AttrGroupDao;
import com.atguigu.gulimall.product.dao.SkuSaleAttrValueDao;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.atguigu.gulimall.product.vo.web.SkuItemSaleAttrVo;
import com.atguigu.gulimall.product.vo.web.SpuItemAttrGroupVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import java.util.List;

@SpringBootTest
class GulimallProductApplicationTests {

	@Autowired
	private BrandService brandService;

	@Autowired
	private StringRedisTemplate stringRedisTemplate;

	@Autowired
	private RedissonClient redissonClient;

	@Autowired
	private AttrGroupDao attrGroupDao;

	@Autowired
	private SkuSaleAttrValueDao skuSaleAttrValueDao;

	@Test
	public void contextLoads() {
		BrandEntity brandEntity = new BrandEntity();

//		brandEntity.setName("华为");
//		brandService.save(brandEntity);
//		System.out.println("保存成功...");

		List<BrandEntity> list = brandService.list(new QueryWrapper<BrandEntity>().eq("brand_id",1l));
		list.forEach((item) -> {
			System.out.println(item);
		});
	}

	@Test
	public void testRedis() {
		ValueOperations<String, String> operations =
				stringRedisTemplate.opsForValue();
		operations.set("hello","hello world");
	}

	@Test
	public void testRedisson() {
		System.out.println(redissonClient);
	}

	@Test
	public void testAttrGroupDao() {
		List<SpuItemAttrGroupVo> attrGroupWithSpuId = attrGroupDao.getAttrGroupWithSpuId(13l, 225l);
		System.out.println(attrGroupWithSpuId);
	}

	@Test
	public void testSkuSaleAttrValueDao() {
		List<SkuItemSaleAttrVo> saleAttrsBySpuId = skuSaleAttrValueDao.getSaleAttrsBySpuId(13l);
		System.out.println(saleAttrsBySpuId);
	}

}
