package com.atguigu.common.to.es;

import lombok.Data;

/**
 * @author cyl
 * @date 2021/11/27/21:35
 * @description
 */
@Data
public class AttrsEsModel {

    private Long attrId;

    private String attrName;

    private String attrValue;
}
