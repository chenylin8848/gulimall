package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author cyl
 * @date 2021/11/17/22:24
 * @description
 */
@Data
public class SpuBoundsTo {

    private Long SpuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;

}
