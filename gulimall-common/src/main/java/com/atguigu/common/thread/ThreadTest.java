package com.atguigu.common.thread;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author cyl
 * @date 2021/12/11/21:55
 * @description
 */
public class ThreadTest {

    public static ExecutorService threadPool = Executors.newFixedThreadPool(10);

    /**
     * 1、初始化线程的 4 种方式
     * 1）、继承 Thread
     * 2）、实现 Runnable 接口
     * 3）、实现 Callable 接口 + FutureTask （可以拿到返回结果，可以处理异常）
     * 4）、线程池
     *
     * @param args
     */
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main...start...");
        System.out.println("main...线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//        Thread thread = new ThreadO1();

//        Runnable01 runnable01 = new Runnable01();
//        Thread thread = new Thread(runnable01);

//        Callable01 callable01 = new Callable01();
//        FutureTask futureTask = new FutureTask<>(callable01);
//        Thread thread = new Thread(futureTask);
//        thread.start();
//        Object o = futureTask.get();//等待整个线程执行，返回结果--阻塞等待
//        System.out.println("main...end..." + o);

//        threadPool.execute(new Runnable01());

//        ThreadPoolExecutor executor = new ThreadPoolExecutor();
        System.out.println("main...end...");
    }

    public static class ThreadO1 extends Thread {
        @Override
        public void run() {
            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
            int i = 10 / 2;
            System.out.println("运行结果:" + i);
        }
    }

    public static class Runnable01 implements Runnable {
        @Override
        public void run() {
            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
            int i = 10 / 2;
            System.out.println("运行结果:" + i);
        }
    }

    public static class Callable01 implements Callable<Integer> {
        @Override
        public Integer call() throws Exception {
            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
            int i = 10 / 2;
            System.out.println("运行结果:" + i);
            return i;
        }
    }
}
