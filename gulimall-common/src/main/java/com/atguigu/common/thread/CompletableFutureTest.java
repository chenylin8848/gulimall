package com.atguigu.common.thread;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author cyl
 * @date 2021/12/12/21:52
 * @description
 */
public class CompletableFutureTest {

    public static ExecutorService threadPool = Executors.newFixedThreadPool(10);

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        System.out.println("main...start...");
        System.out.println("main...线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());

//        CompletableFuture<Void> completableFuture = CompletableFuture.runAsync(() -> {
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            System.out.println("运行结果:" + i);
//        }, threadPool);

//        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool);
//
//        Integer integer = completableFuture.get();
//        System.out.println("main...end..." + integer);

//        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
////            int i = 10 / 2;
//            int i = 10 / 0;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool).whenComplete((t,u)->{
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            System.out.println("异步任务成功完成了...结果是:" + t);
//            System.out.println("异常是:" + u);
//        }).exceptionally(throwable -> {
//            return 10;
//        });
//        Integer integer = completableFuture.get();
//        System.out.println("main...end..." + integer);

//        CompletableFuture<Integer> completableFuture = CompletableFuture.supplyAsync(() -> {
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
////            int i = 10 / 2;
//            int i = 10 / 0;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool).handle((t,u) -> {
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            System.out.println("异步任务成功完成了...结果是:" + t);
//            System.out.println("异常是:" + u);
//            if (t != null) {
//                return t * 2;
//            } else {
//                return 0;
//            }
//        });
//        Integer integer = completableFuture.get();
//        System.out.println("main...end..." + integer);

//        CompletableFuture.supplyAsync(() -> {
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    int i = 10 / 2;
////            int i = 10 / 0;
//                    System.out.println("运行结果:" + i);
//                    return i;
//                }, threadPool)
        //不能获取到上一步的执行结果
//                .thenRunAsync(() -> {
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            System.out.println("任务2启动了....");
//        },threadPool);
        //能获取到上一步的执行结果
//        .thenAcceptAsync(t->{
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            System.out.println("任务2启动了....上一步执行结果:" + t);
//        },threadPool);
        //能获取到上一步的执行结果并返回值
//                .thenApplyAsync(t -> {
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    System.out.println("任务2启动了....上一步执行结果:" + t);
//                    return t * 2;
//                }, threadPool);

//        CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool).runAfterBoth(CompletableFuture.supplyAsync(()->{
//            System.out.println("任务2启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            return i;
//        },threadPool),()->{
//            System.out.println("两个任务都完成...");
//        });

//        CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool).thenAcceptBothAsync(
//                CompletableFuture.supplyAsync(()->{
//                    System.out.println("任务2启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    int i = 10 / 5;
//                    System.out.println("运行结果:" + i);
//                    return i;
//                },threadPool),
//                (t,u)->{
//                    System.out.println("任务3启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    System.out.println("上一步的执行结果:" + t + "-->" + u);
//                }
//                ,threadPool);

//        CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool).thenCombineAsync(
//                CompletableFuture.supplyAsync(() -> {
//                            System.out.println("任务2启动了...");
//                            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                            int i = 10 / 5;
//                            System.out.println("运行结果:" + i);
//                            return i;
//                        },
//                        threadPool), (t, u) -> {
//                    System.out.println("任务3启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    System.out.println("上一步的执行结果:" + t + "-->" + u);
//
//                    return t + u;
//                },
//                threadPool);

//        CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool).runAfterEitherAsync(CompletableFuture.supplyAsync(()->{
//            System.out.println("任务2启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 5;
//            System.out.println("运行结果:" + i);
//            return i;
//        },threadPool),()->{
//            System.out.println("任务3启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//        },threadPool);

//        CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool).acceptEitherAsync(CompletableFuture.supplyAsync(() -> {
//                    System.out.println("任务2启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    int i = 10 / 5;
//                    try {
//                        Thread.sleep(3000);
//                        System.out.println("任务2结束了...");
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println("运行结果:" + i);
//                    return i;
//                }, threadPool),
//                t -> {
//                    System.out.println("任务3启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    System.out.println("上一步执行结果:" + t);
//                },
//                threadPool);

//        CompletableFuture.supplyAsync(() -> {
//            System.out.println("任务1启动了...");
//            System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//            int i = 10 / 2;
//            System.out.println("运行结果:" + i);
//            return i;
//        }, threadPool).applyToEitherAsync(CompletableFuture.supplyAsync(() -> {
//                    System.out.println("任务2启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    int i = 10 / 5;
//                    try {
//                        Thread.sleep(3000);
//                        System.out.println("任务2结束了...");
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                    System.out.println("运行结果:" + i);
//                    return i;
//                }, threadPool),t->{
//                    System.out.println("任务3启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    System.out.println("上一步执行结果:" + t);
//                    return t;
//        },threadPool);

        /**
         * 多任务组合
         */
//        CompletableFuture.allOf(CompletableFuture.supplyAsync(() -> {
//                    System.out.println("任务1启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    int i = 10 / 2;
//                    System.out.println("运行结果:" + i);
//                    return i;
//                }, threadPool), CompletableFuture.supplyAsync(() -> {
//                    System.out.println("任务2启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    int i = 10 / 5;
//                    System.out.println("运行结果:" + i);
//                    return i;
//                }, threadPool), CompletableFuture.supplyAsync(() -> {
//                    System.out.println("任务3启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    int i = 10 / 5;
//                    System.out.println("运行结果:" + i);
//                    return i;
//                }, threadPool), CompletableFuture.supplyAsync(() -> {
//                    System.out.println("任务4启动了...");
//                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
//                    int i = 10 / 5;
//                    System.out.println("运行结果:" + i);
//                    return i;
//                }, threadPool)
//        ).get();

        CompletableFuture.anyOf(CompletableFuture.supplyAsync(() -> {
                    System.out.println("任务1启动了...");
                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
                    int i = 10 / 2;
                    System.out.println("运行结果:" + i);
                    return i;
                }, threadPool), CompletableFuture.supplyAsync(() -> {
                    System.out.println("任务2启动了...");
                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
                    int i = 10 / 5;
                    System.out.println("运行结果:" + i);
                    return i;
                }, threadPool), CompletableFuture.supplyAsync(() -> {
                    System.out.println("任务3启动了...");
                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
                    int i = 10 / 5;
                    System.out.println("运行结果:" + i);
                    return i;
                }, threadPool), CompletableFuture.supplyAsync(() -> {
                    System.out.println("任务4启动了...");
                    System.out.println("当前线程:" + Thread.currentThread().getId() + "-->" + Thread.currentThread().getName());
                    int i = 10 / 5;
                    System.out.println("运行结果:" + i);
                    return i;
                }, threadPool)
        )
//                .get();
        .join();

        System.out.println("main...end...");
    }
}
