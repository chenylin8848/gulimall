package com.atguigu.common.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.constraints.NotNull;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author cyl
 * @date 2021/11/10/10:57
 * @description 自定义校验注解
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(
        validatedBy = {ListConstrainValidator.class}
)
public @interface ListValid {

    String message() default "{com.atguigu.common.valid.ListValid}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    //数组，需要用户指定值
    int[] value() default {};
}
