package com.atguigu.common.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.HashSet;
import java.util.Set;

/**
 * @author cyl
 * @date 2021/11/10/11:06
 * @description
 */
public class ListConstrainValidator implements ConstraintValidator<ListValid, Integer> {

    private Set<Integer> set = new HashSet<>();

    @Override
    public void initialize(ListValid constraintAnnotation) {
        int[] value = constraintAnnotation.value();

        for (Integer v:value) {
            set.add(v);
        }
    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return set.contains(integer);
    }
}
