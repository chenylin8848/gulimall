package com.atguigu.common.enmu;

/**
 * @author cyl
 * @date 2021/11/10/10:25
 * @description 统一返回code
 */
public enum SystemCodeEnum {

    CODE_ENUM_SUCCESS(200,"成功"),
    CODE_ENUM_NOAUTH(401,"没有权限"),
    CODE_ENUM_PARAMVALID_FAILURE(400,"参数校验异常");

    private Integer code;
    private String message;

    private SystemCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
