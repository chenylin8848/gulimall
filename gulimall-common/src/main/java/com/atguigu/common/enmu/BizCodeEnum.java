package com.atguigu.common.enmu;

/**
 * @author cyl
 * @date 2021/11/10/10:25
 * @description 统一返回code
 */

/**
 * 11xxx 商品
 * 12xxx 订单
 * 13xxx 购物车
 * 14xxx 物流
 */
public enum BizCodeEnum {

    PRODUCT_UP_EXCEPTION(11000,"商品上架异常");

    private Integer code;
    private String message;

    private BizCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }
}
