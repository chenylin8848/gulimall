package com.atguigu.common.constant;

/**
 * @author cyl
 * @date 2021/11/14/21:36
 * @description
 */
public class ProductConstant {

    public enum AttrEnum {
        ATTR_TYPE_BASE(1,"基本属性"),
        ATTR_TYPE_SALE(0,"销售属性"),

        ATTR_SEARCH_YES(1,"可检索"),
        ATTR_SEARCH_NO(0,"不可检索");

        private int code;
        private String message;

        AttrEnum(int code,String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }
    }

    public enum StatusEnum {
        NEW_SPU(0,"新建"),
        UP_SPU(1,"商品上架"),

        DOWN_SPU(2,"商品下架");

        private int code;
        private String message;

        StatusEnum(int code,String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }
    }
}
