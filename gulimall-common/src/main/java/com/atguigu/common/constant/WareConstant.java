package com.atguigu.common.constant;

/**
 * @author cyl
 * @date 2021/11/19/21:03
 * @description
 */
public class WareConstant {

    public enum PurchaseStatusEnum {
        CREATED(0,"新建"),
        ASSIGNED(1,"已分配"),
        RECEIVED(2,"已领取"),
        FINISHED(3,"已完成"),
        ERROR(4,"异常");

        private int code;
        private String message;

        PurchaseStatusEnum(int code,String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }
    }

    public enum PurchaseDeatilStatusEnum {
        CREATED(0,"新建"),
        ASSIGNED(1,"已分配"),
        BUYING(2,"正在采购"),
        FINISHED(3,"已完成"),
        FAILURE(4,"采购失败");

        private int code;
        private String message;

        PurchaseDeatilStatusEnum(int code,String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return this.code;
        }

        public String getMessage() {
            return this.message;
        }
    }
}
