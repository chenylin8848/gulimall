import com.alibaba.fastjson.JSON;
import com.atguigu.gulimall.search.GulimallSearchApplication;
import lombok.Data;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.Avg;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * @author cyl
 * @date 2021/11/26/22:43
 * @description
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = GulimallSearchApplication.class)
public class GulimallSearchTest {

    @Autowired
    private RestHighLevelClient client;

    /**
     * 测试客户端
     */
    @Test
    public void testClient() {
        System.out.println(client);
    }

    /**
     * 测试存储数据到es
     */
    @Test
    public void testIndex() throws IOException {
        IndexRequest indexRequest = new IndexRequest("user");
        indexRequest.id("1");
//        indexRequest.source("userName","zhangsan").source("age",18).source("gender","男");

        User user = new User();
        user.setUserName("zhangsan");
        user.setAge(18);
        user.setGender("男");
        String jsonString = JSON.toJSONString(user);
        indexRequest.source(jsonString, XContentType.JSON);

        /**
         * 同步保存
         */
        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);

        /**
         * 异步保存
         */
//        client.indexAsync(indexRequest, RequestOptions.DEFAULT, new ActionListener<IndexResponse>() {
//            @Override
//            public void onResponse(IndexResponse indexResponse) {
//
//            }
//
//            @Override
//            public void onFailure(Exception e) {
//
//            }
//        });

    }

    @Test
    public void testSearch() throws IOException {
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices("bank"); //指定索引

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
//        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchSourceBuilder.query(QueryBuilders.matchQuery("address","mill"));
//        searchSourceBuilder.from(0);
//        searchSourceBuilder.size(3);

        //按照年龄值分布进行聚合
        TermsAggregationBuilder ageAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
        searchSourceBuilder.aggregation(ageAgg);

        AvgAggregationBuilder balanceAvg = AggregationBuilders.avg("balanceAvg").field("balance");
        searchSourceBuilder.aggregation(balanceAvg);

        System.out.println("searchSourceBuilder=" + searchSourceBuilder);
        searchRequest.source(searchSourceBuilder); //指定DSL,检索条件

        SearchResponse search = client.search(searchRequest, RequestOptions.DEFAULT);

//        System.out.println(search);

//        Map map = JSON.parseObject(search.toString(), Map.class);
        SearchHits hits = search.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit hit : searchHits) {
            String sourceAsString = hit.getSourceAsString();
            System.out.println(sourceAsString);
        }

        Aggregations aggregations = search.getAggregations();
//        for (Aggregation aggregation : aggregations) {
//            System.out.println(aggregation.getName());
//        }
        Terms agg = aggregations.get("ageAgg");
        for (Terms.Bucket bucket : agg.getBuckets()) {
            System.out.println("age:" + bucket.getKeyAsString() + "count:" + bucket.getDocCount());
        }

        Avg avg = aggregations.get("balanceAvg");
        System.out.println("balance:" + avg.getValue());
    }

    @Test
    public void testString() {
        String hello = "高通(Qualcomm)";

        try {
            String encode = URLEncoder.encode(hello,"UTF-8");
            System.out.println("encode==" + encode);
            encode = encode.replace("%28", "(");
            System.out.println("encode==" + encode);
//            System.out.println(encode.replace("%28", "("));
            encode = encode.replaceAll("%29", ")");
//            System.out.println(encode.replaceAll("%29", ")"));
            System.out.println("encode==" + encode);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    @Data
    class User{
        private String userName;
        private Integer age;
        private String gender;
    }
}
