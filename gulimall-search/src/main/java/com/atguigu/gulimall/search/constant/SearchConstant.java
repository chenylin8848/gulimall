package com.atguigu.gulimall.search.constant;

/**
 * @author cyl
 * @date 2021/11/27/22:52
 * @description
 */
public class SearchConstant {

    public static final String PRODUCT_INDEX = "gulimall_product"; //sku数据在es中的索引

    public static final Integer PRODUCT_PAGESIZE = 8; //分页
}
