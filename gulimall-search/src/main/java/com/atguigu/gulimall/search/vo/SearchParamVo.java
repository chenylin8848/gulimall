package com.atguigu.gulimall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * 封装页面所有可能传递过来的查询条件
 * @author cyl
 * @date 2021/12/05/21:32
 * @description
 */
@Data
public class SearchParamVo {

    /**
     * 页面传递过来的全文匹配关键字
     */
    private String keyword;

    /**
     * 三级分类id
     */
    private Long catalog3Id;

    /**
     * 排序条件
     *  sort=saleCount_asc/desc
     *  sort=skuPrice_asc/desc
     *  sort=hotSore_asc/desc
     */
    private String sort;

    /**
     * hasStock(是否有货) hasStock=0/1 0-无库存 1-有库存
     */
    private Integer hasStock = 1;

    /**
     * skuPrice(价格区间) skuPrice=1_500/_500/500_
     */
    private String skuPrice;

    /**
     * 品牌id--支持多选
     *  brandId=1&brandId=2
     */
    private List<Long> brandId;

    /**
     * 按照属性进行筛选
     *  attrs=1_其他:安卓&attrs=2_5存:6存
     */
    private List<String> attrs;

    /**
     * 页码
     */
    private Integer pageNum = 1;

    /**
     * 原生的所有查询条件
     */
    private String _queryString;

}
