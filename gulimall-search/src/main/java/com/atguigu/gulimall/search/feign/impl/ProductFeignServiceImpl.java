package com.atguigu.gulimall.search.feign.impl;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.search.feign.ProductFeignService;

import java.util.List;

/**
 * @author cyl
 * @date 2021/12/09/21:42
 * @description
 */
public class ProductFeignServiceImpl implements ProductFeignService {
    @Override
    public R info(Long attrId) {
        return null;
    }

    @Override
    public R infos(List<Long> brandIds) {
        return null;
    }
}
