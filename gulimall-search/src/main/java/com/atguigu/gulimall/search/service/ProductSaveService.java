package com.atguigu.gulimall.search.service;

import com.atguigu.common.to.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @author cyl
 * @date 2021/11/27/22:49
 * @description
 */
public interface ProductSaveService {

    boolean productUp(List<SkuEsModel> skuEsModelList) throws IOException;
}
