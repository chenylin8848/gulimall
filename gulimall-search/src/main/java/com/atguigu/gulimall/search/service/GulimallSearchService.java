package com.atguigu.gulimall.search.service;

import com.atguigu.gulimall.search.vo.SearchParamVo;
import com.atguigu.gulimall.search.vo.SearchResultVo;

/**
 * @author cyl
 * @date 2021/12/05/21:33
 * @description
 */
public interface GulimallSearchService {

    /**
     * @param vo 检索的所有参数
     * @return 检索结果
     */
    public SearchResultVo search(SearchParamVo vo);
}
