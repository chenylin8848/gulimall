package com.atguigu.gulimall.search.web;

import com.atguigu.gulimall.search.service.GulimallSearchService;
import com.atguigu.gulimall.search.vo.SearchParamVo;
import com.atguigu.gulimall.search.vo.SearchResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * @author cyl
 * @date 2021/11/28/16:24
 * @description
 */
@Controller
public class SearchController {

    @Autowired
    private GulimallSearchService gulimallSearchService;

    @GetMapping("/list.html")
    public String indexPage(SearchParamVo vo,
                            Model model,
                            HttpServletRequest request) {
        vo.set_queryString(request.getQueryString());
        SearchResultVo result = gulimallSearchService.search(vo);
        model.addAttribute("result",result);

        return "list";
    }


}
