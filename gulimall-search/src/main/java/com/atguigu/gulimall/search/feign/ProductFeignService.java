package com.atguigu.gulimall.search.feign;

import com.atguigu.common.utils.R;
import com.atguigu.gulimall.search.feign.impl.ProductFeignServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author cyl
 * @date 2021/12/09/21:42
 * @description
 */
@FeignClient(value = "gulimall-product",fallback = ProductFeignServiceImpl.class)
public interface ProductFeignService {

    /**
     * 信息
     */
    @RequestMapping("/product/attr/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId);

    /**
     * 信息
     */
    @RequestMapping("/product/brand/infos")
    public R infos(@RequestParam("brandIds") List<Long> brandIds);
}
