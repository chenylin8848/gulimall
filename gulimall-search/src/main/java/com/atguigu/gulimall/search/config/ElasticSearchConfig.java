package com.atguigu.gulimall.search.config;

import lombok.Data;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author cyl
 * @date 2021/11/26/22:29
 * @description
 */
@Configuration
@ConfigurationProperties(prefix = "elastic.search")
@Data
public class ElasticSearchConfig {

    private String host;

    private Integer port;

    public static final RequestOptions COMMON_OPTIONS;

    static {
        RequestOptions.Builder builder = RequestOptions.DEFAULT.toBuilder();
        COMMON_OPTIONS = builder.build();
    }

    @Bean
    public RestHighLevelClient getEsClient() {
        RestClientBuilder builder =
                RestClient.builder(new HttpHost(host, port, "http"));
//                RestClient.builder(new HttpHost("47.106.166.2", 9200, "http"));
        return new RestHighLevelClient(builder);
    }
}
