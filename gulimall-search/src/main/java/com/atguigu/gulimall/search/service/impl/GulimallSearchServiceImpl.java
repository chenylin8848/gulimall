package com.atguigu.gulimall.search.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.atguigu.common.to.es.SkuEsModel;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.search.config.ElasticSearchConfig;
import com.atguigu.gulimall.search.constant.SearchConstant;
import com.atguigu.gulimall.search.feign.ProductFeignService;
import com.atguigu.gulimall.search.service.GulimallSearchService;
import com.atguigu.gulimall.search.vo.AttrResponseVo;
import com.atguigu.gulimall.search.vo.BrandVo;
import com.atguigu.gulimall.search.vo.SearchParamVo;
import com.atguigu.gulimall.search.vo.SearchResultVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.nested.NestedAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.nested.ParsedNested;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedLongTerms;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author cyl
 * @date 2021/12/05/21:33
 * @description
 */
@Service
@Slf4j
public class GulimallSearchServiceImpl implements GulimallSearchService {

    @Autowired
    private RestHighLevelClient client;

    @Autowired
    private ProductFeignService productFeignService;

    @Override
    public SearchResultVo search(SearchParamVo vo) {
        SearchResultVo result = new SearchResultVo();

        //自动构建DSL语句
        //1.准备检索请求
        SearchRequest searchRequest = buildSearchRequest(vo);


        try {
            //2.执行检索请求
            SearchResponse response = client.search(searchRequest, ElasticSearchConfig.COMMON_OPTIONS);

            //3.分析响应数据,封装成我们想要的数据
            return buildSearchResponse(response, vo);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 构建请求数据
     *
     * @return
     */
    private SearchRequest buildSearchRequest(SearchParamVo vo) {
        //构建DSL语句
        SearchSourceBuilder builder = new SearchSourceBuilder();

        /**
         * 模糊匹配、过滤（按照属性、分类、品牌、价格区间、库存）
         */
        //1.构建bool
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        //1.1构建must
        if (StringUtils.isNotBlank(vo.getKeyword())) {
            boolQuery.must(QueryBuilders.matchQuery("skuTitle", vo.getKeyword()));
        }
        //1.2构建filter
        //1.2.1三级分类
        if (vo.getCatalog3Id() != null) {
            boolQuery.filter(QueryBuilders.termQuery("catalogId", vo.getCatalog3Id()));
        }
        //1.2.2品牌id
        if (vo.getBrandId() != null && vo.getBrandId().size() > 0) {
            boolQuery.filter(QueryBuilders.termsQuery("brandId", vo.getBrandId()));
        }
        //1.2.3属性
        if (vo.getAttrs() != null && vo.getAttrs().size() > 0) {
            for (String attr : vo.getAttrs()) {
                BoolQueryBuilder boolQuery1 = QueryBuilders.boolQuery();
                String[] s = attr.split("_");
                //属性id
                String attrId = s[0];
                //属性检索用的值
                String[] attrValues = s[1].split(":");
                boolQuery1.must(QueryBuilders.termQuery("attrs.attrId", attrId));
                boolQuery1.must(QueryBuilders.termsQuery("attrs.attrValue", attrValues));
                //todo 以下代码放在循环外会怎么样？测试一下
                NestedQueryBuilder nestedQuery = QueryBuilders.nestedQuery("attrs", boolQuery1, ScoreMode.None);
                boolQuery.filter(nestedQuery);
            }
        }

        //1.2.4库存 0-无库存 1-有库存
        boolQuery.filter(QueryBuilders.termsQuery("hasStock", vo.getHasStock() == 1));
        //1.2.5价格区间 skuPrice=1_500/_500/500_
        if (StringUtils.isNotBlank(vo.getSkuPrice())) {
            RangeQueryBuilder rangeQuery = QueryBuilders.rangeQuery("skuPrice");
            String[] s = vo.getSkuPrice().split("_");
            if (s.length == 2) {
                rangeQuery.gte(s[0]).lte(s[1]);
            } else {
                if (vo.getSkuPrice().startsWith("_")) {
                    rangeQuery.lte(s[0]);
                }
                if (vo.getSkuPrice().endsWith("_")) {
                    rangeQuery.gte(s[0]);
                }
            }
            boolQuery.filter(rangeQuery);
        }

        builder.query(boolQuery);

        /**
         * 排序、分页、高亮
         */
        //2.1排序
        // sort=saleCount_asc/desc
        // sort=skuPrice_asc/desc
        // sort=hotSore_asc/desc
        if (StringUtils.isNotBlank(vo.getSort())) {
            String[] s = vo.getSort().split("_");
            SortOrder sortOrder = s[1].equalsIgnoreCase("asc") ? SortOrder.ASC : SortOrder.DESC;
            builder.sort(s[0], sortOrder);
        }

        //2.2分页 pageSize=2
        // pageNum: 1 from:0 size:2
        // pageNum: 2 from:2 size:2
        builder.from((vo.getPageNum() - 1) * SearchConstant.PRODUCT_PAGESIZE);
        builder.size(SearchConstant.PRODUCT_PAGESIZE);

        //2.3高亮
        if (StringUtils.isNotBlank(vo.getKeyword())) {
            HighlightBuilder highlightBuilder = new HighlightBuilder();
            highlightBuilder.field("skuTitle");
            highlightBuilder.preTags("<b style='color: red'>");
            highlightBuilder.postTags("</b>");
            builder.highlighter(highlightBuilder);
        }


        /**
         * 聚合分析
         */
        //3.1品牌聚合
        TermsAggregationBuilder brand_agg = AggregationBuilders.terms("brand_agg");
        brand_agg.field("brandId").size(50);

        //3.1.1品牌名字子聚合
        TermsAggregationBuilder brand_name_agg = AggregationBuilders.terms("brand_name_agg").field("brandName").size(1);
        brand_agg.subAggregation(brand_name_agg);
        //3.1.2品牌名字子聚合
        TermsAggregationBuilder brand_img_agg = AggregationBuilders.terms("brand_img_agg").field("brandImg").size(1);
        brand_agg.subAggregation(brand_img_agg);

        builder.aggregation(brand_agg);
        //3.2分类聚合
        TermsAggregationBuilder catalog_agg = AggregationBuilders.terms("catalog_agg");
        catalog_agg.field("catalogId").size(50);
        //3.2.1分类名字子聚合
        TermsAggregationBuilder catalog_name_agg = AggregationBuilders.terms("catalog_name_agg").field("catalogName").size(1);
        catalog_agg.subAggregation(catalog_name_agg);

        builder.aggregation(catalog_agg);

        //3.3属性聚合
        NestedAggregationBuilder attr_agg = AggregationBuilders.nested("attr_agg", "attrs");
        //3.3.1属性子聚合
        TermsAggregationBuilder attr_id_agg = AggregationBuilders.terms("attr_id_agg").field("attrs.attrId").size(50);
        TermsAggregationBuilder attr_name_agg = AggregationBuilders.terms("attr_name_agg").field("attrs.attrName").size(1);
        attr_id_agg.subAggregation(attr_name_agg);

        TermsAggregationBuilder attr_value_agg = AggregationBuilders.terms("attr_value_agg").field("attrs.attrValue").size(50);
        attr_id_agg.subAggregation(attr_value_agg);

        attr_agg.subAggregation(attr_id_agg);

        builder.aggregation(attr_agg);

        log.info("构建的DSL语句,{}", builder.toString());

        SearchRequest searchRequest = new SearchRequest(new String[]{SearchConstant.PRODUCT_INDEX}, builder);

        return searchRequest;
    }

    /**
     * 构建结果数据
     *
     * @param response
     * @return
     */
    private SearchResultVo buildSearchResponse(SearchResponse response, SearchParamVo vo) {
        log.info("检索得到的结果,{}", response);
        SearchResultVo resultVo = new SearchResultVo();

        SearchHits hits = response.getHits();

        List<SkuEsModel> skuEsModelList = new ArrayList<>();

        if (hits.getHits() != null && hits.getHits().length > 0) {
            for (SearchHit hit : hits.getHits()) {
                String sourceAsString = hit.getSourceAsString();
                SkuEsModel skuEsModel = JSONObject.parseObject(sourceAsString, SkuEsModel.class);
                if (StringUtils.isNotBlank(vo.getKeyword())) {
                    HighlightField skuTitle = hit.getHighlightFields().get("skuTitle");
                    String string = skuTitle.getFragments()[0].string();
                    skuEsModel.setSkuTitle(string);
                }
                skuEsModelList.add(skuEsModel);
            }
        }

        //1.返回的所有查询到的商品
        resultVo.setProducts(skuEsModelList);

        Aggregations aggregations = response.getAggregations();

        //2.当前所有商品涉及到的所有属性信息
        List<SearchResultVo.AttrVo> attrVoList = new ArrayList<>();
        ParsedNested attr_agg = aggregations.get("attr_agg");
        ParsedLongTerms attr_id_agg = attr_agg.getAggregations().get("attr_id_agg");
        List<? extends Terms.Bucket> attr_id_agg_buckets = attr_id_agg.getBuckets();
        for (Terms.Bucket bucket : attr_id_agg_buckets) {
            SearchResultVo.AttrVo attrVo = new SearchResultVo.AttrVo();
            attrVo.setAttrId(bucket.getKeyAsNumber().longValue());

            ParsedStringTerms attr_name_agg = bucket.getAggregations().get("attr_name_agg");
            List<? extends Terms.Bucket> buckets1 = attr_name_agg.getBuckets();
            Terms.Bucket bucket1 = buckets1.get(0);
            attrVo.setAttrName(bucket1.getKeyAsString());

            ParsedStringTerms attr_value_agg = bucket.getAggregations().get("attr_value_agg");
            List<? extends Terms.Bucket> buckets2 = attr_value_agg.getBuckets();
            List<String> stringList = buckets2.stream().map(item -> {
                return item.getKeyAsString();
            }).collect(Collectors.toList());
            attrVo.setAttrValue(stringList);

            attrVoList.add(attrVo);
        }
        resultVo.setAttrs(attrVoList);

        //3.当前所有商品涉及到的所有品牌信息
        List<SearchResultVo.BrandVo> brandVoList = new ArrayList<>();
        ParsedLongTerms brand_agg = aggregations.get("brand_agg");
        List<? extends Terms.Bucket> brand_agg_buckets = brand_agg.getBuckets();
        for (Terms.Bucket bucket : brand_agg_buckets) {
            SearchResultVo.BrandVo brandVo = new SearchResultVo.BrandVo();
            brandVo.setBrandId(bucket.getKeyAsNumber().longValue());

            ParsedStringTerms brand_img_agg = bucket.getAggregations().get("brand_img_agg");
            List<? extends Terms.Bucket> buckets1 = brand_img_agg.getBuckets();
            Terms.Bucket bucket1 = buckets1.get(0);
            brandVo.setBrandImg(bucket1.getKeyAsString());

            ParsedStringTerms brand_name_agg = bucket.getAggregations().get("brand_name_agg");
            List<? extends Terms.Bucket> buckets2 = brand_name_agg.getBuckets();
            Terms.Bucket bucket2 = buckets2.get(0);
            brandVo.setBrandName(bucket2.getKeyAsString());
            brandVoList.add(brandVo);
        }
        resultVo.setBrands(brandVoList);

        //4.当前所有商品涉及到的所有分类信息
        List<SearchResultVo.CatalogVo> catalogVoList = new ArrayList<>();
        ParsedLongTerms catalog_agg = aggregations.get("catalog_agg");
        List<? extends Terms.Bucket> catalog_agg_buckets = catalog_agg.getBuckets();
        for (Terms.Bucket bucket : catalog_agg_buckets) {
            SearchResultVo.CatalogVo catalogVo = new SearchResultVo.CatalogVo();
            catalogVo.setCatalogId(bucket.getKeyAsNumber().longValue());

            ParsedStringTerms catalog_name_agg = bucket.getAggregations().get("catalog_name_agg");
            List<? extends Terms.Bucket> buckets1 = catalog_name_agg.getBuckets();
            Terms.Bucket bucket1 = buckets1.get(0);
            catalogVo.setCatalogName(bucket1.getKeyAsString());
            catalogVoList.add(catalogVo);
        }
        resultVo.setCatalogs(catalogVoList);

        //5.分页信息-页码
        resultVo.setPageNum(vo.getPageNum());

        //6.分页信息-总记录数
        long totalCount = hits.getTotalHits().value;
        resultVo.setTotal(totalCount);

        //7.分页信息-总页码
        int totalPages =
                totalCount % SearchConstant.PRODUCT_PAGESIZE == 0 ? (int) (totalCount / SearchConstant.PRODUCT_PAGESIZE) : (int) (totalCount / SearchConstant.PRODUCT_PAGESIZE + 1);
        resultVo.setTotalPages(totalPages);

        //8.分页信息-中间页码数
        List<Integer> pageNavs = new ArrayList<>();
        for (int i = 1; i < totalPages; i++) {
            pageNavs.add(i);
        }
        resultVo.setPageNavs(pageNavs);

        //9、构建面包屑导航
        if (vo.getAttrs() != null && vo.getAttrs().size() > 0) {
            List<SearchResultVo.NavVo> collect = vo.getAttrs().stream().map(attr -> {

                //1、分析每一个attrs传过来的参数值
                SearchResultVo.NavVo navVo = new SearchResultVo.NavVo();
                String[] s = attr.split("_");
                navVo.setNavValue(s[1]);

                resultVo.getAttrIds().add(Long.parseLong(s[0]));
                R r = productFeignService.info(Long.parseLong(s[0]));
                if (r.getCode() == 0) {
                    AttrResponseVo data = r.getData("attr", new TypeReference<AttrResponseVo>() {
                    });
                    navVo.setNavName(data.getAttrName());
                } else {
                    navVo.setNavName(s[0]);
                }


                //2、取消了这个面包屑以后，我们要跳转到哪个地方，将请求的地址url里面的当前置空
                //拿到所有的查询条件，去掉当前
                String replace = replaceString(vo, attr,"attrs");
                navVo.setLink("http://sear.gulimall.com/list.html?" + replace);

                return navVo;
            }).collect(Collectors.toList());

            resultVo.setNavs(collect);
        }
        //品牌、分类
        if (vo.getBrandId() != null && vo.getBrandId().size() > 0) {
            List<SearchResultVo.NavVo> navs = resultVo.getNavs();
            SearchResultVo.NavVo navVo = new SearchResultVo.NavVo();
            navVo.setNavName("品牌:");
            //todo 远程查询所有品牌
            R infos = productFeignService.infos(vo.getBrandId());
            if (infos.getCode() == 0) {
                StringBuffer buffer = new StringBuffer();
                List<BrandVo> brandVos = infos.getData(new TypeReference<List<BrandVo>>() {
                });
                String replaceString = "";
                for (BrandVo brnadVo : brandVos) {
                    buffer.append(brnadVo.getName() + ";");
                    replaceString = replaceString(vo, brnadVo.getBrandId() + "", "brandId");
                }
                navVo.setNavValue(buffer.toString());
                navVo.setLink("http://sear.gulimall.com/list.html?" + replaceString);
            }

            navs.add(navVo);
        }


        log.info("检索得到的结果,{}", resultVo.toString());

        return resultVo;
    }

    private String replaceString(SearchParamVo vo, String attr, String key) {
        String encode = null;
        try {
            encode = URLEncoder.encode(attr, "UTF-8");
            encode = encode.replace("+", "%20");  //浏览器对空格的编码和Java不一样，差异化处理
            encode = encode.replace("%28", "(");  //浏览器对(的编码和Java不一样，差异化处理
            encode = encode.replace("%29", ")");  //浏览器对)的编码和Java不一样，差异化处理
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String replace = vo.get_queryString().replace("&" + key + "=" + encode, "");
//        String replace = vo.get_queryString().replace(key + "=" + encode, "");
        return replace;
    }
}
