package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author chenyuanlin
 * @email chenyuanlin@gmail.com
 * @date 2020-11-20 22:20:37
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
