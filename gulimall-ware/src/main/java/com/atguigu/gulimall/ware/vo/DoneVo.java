package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author cyl
 * @date 2021/11/19/22:00
 * @description
 */
@Data
public class DoneVo {

    @NotNull
    private Long id;

    private List<DoneItemVo> items;
}
