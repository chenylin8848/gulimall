package com.atguigu.gulimall.ware.service.impl;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.ware.dao.WareSkuDao;
import com.atguigu.gulimall.ware.entity.WareSkuEntity;
import com.atguigu.gulimall.ware.feign.ProductFeignService;
import com.atguigu.gulimall.ware.service.WareSkuService;
import com.atguigu.gulimall.ware.vo.SkuHasStockVo;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    private ProductFeignService productFeignService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        LambdaQueryWrapper<WareSkuEntity> queryWrapper = Wrappers.lambdaQuery();

        String skuId = (String) params.get("skuId");
        String wareId = (String) params.get("wareId");

        if (StringUtils.isNotBlank(skuId)) {
            queryWrapper.eq(WareSkuEntity::getSkuId,skuId);
        }
        if (StringUtils.isNotBlank(wareId)) {
            queryWrapper.eq(WareSkuEntity::getWareId,wareId);
        }


        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Transactional
    @Override
    public void addStock(Long skuId, Long wareId, Integer stock) {
        LambdaQueryWrapper<WareSkuEntity> lambdaQuery = Wrappers.lambdaQuery();
        lambdaQuery.eq(WareSkuEntity::getSkuId,skuId);
        lambdaQuery.eq(WareSkuEntity::getWareId,wareId);
        WareSkuEntity one = this.getOne(lambdaQuery);

        if (one == null) {
            one = new WareSkuEntity();
            one.setSkuId(skuId);
            one.setWareId(wareId);
            one.setStock(stock);
            one.setStockLocked(0);

            //如果失败，事务无需回滚
            //1.自己catch异常
            //2.todo 还可以用什么办法让异常出现事务不回滚？
            try {
                R r = productFeignService.getSkuInfoById(skuId);
                if (r.getCode() == 0) {
                    Map<String,Object> skuInfo = (Map<String, Object>) r.get("skuInfo");
                    one.setSkuName((String) skuInfo.get("skuName"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            this.save(one);
        } else {
            one.setStock(one.getStock()+stock);
            this.updateById(one);
        }

    }

    @Override
    public List<SkuHasStockVo> getSkuHasStock(List<Long> skuIds) {

        List<SkuHasStockVo> skuHasStockVos = skuIds.stream().map(item -> {
            SkuHasStockVo stockVo = new SkuHasStockVo();
            stockVo.setSkuId(item);

            //SELECT SUM(stock - stock_locked) FROM wms_ware_sku WHERE sku_id = 1
            Long count = baseMapper.getSkuStock(item);

            stockVo.setHasStock(count == null?false:count>0);

            return stockVo;
        }).collect(Collectors.toList());
        return skuHasStockVos;
    }

}