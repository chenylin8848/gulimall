package com.atguigu.gulimall.ware.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author cyl
 * @date 2021/11/19/22:41
 * @description
 */
@FeignClient("gulimall-product")
public interface ProductFeignService {

    /**
     * 信息
     */
    @RequestMapping("/product/skuinfo/info/{skuId}")
//    @RequestMapping("/api/product/skuinfo/info/{skuId}") @FeignClient("gulimall-gateway")
    public R getSkuInfoById(@PathVariable("skuId") Long skuId);
}
