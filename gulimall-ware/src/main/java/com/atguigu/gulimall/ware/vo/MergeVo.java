package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author cyl
 * @date 2021/11/19/20:59
 * @description 合并采购单
 */
@Data
public class MergeVo {

    private Long purchaseId;

    private List<Long> items;
}
