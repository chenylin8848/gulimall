package com.atguigu.gulimall.ware.service;

import com.atguigu.gulimall.ware.vo.DoneVo;
import com.atguigu.gulimall.ware.vo.MergeVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.ware.entity.PurchaseEntity;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author chenyuanlin
 * @email chenyuanlin@gmail.com
 * @date 2020-11-20 22:20:37
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    PageUtils queryPageUnreceivePurchase(Map<String, Object> params);

    void merge(MergeVo vo);

    void received(List<Long> ids);

    void done(DoneVo vo) throws Exception;
}

