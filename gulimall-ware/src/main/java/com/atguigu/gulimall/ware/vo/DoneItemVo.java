package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @author cyl
 * @date 2021/11/19/22:01
 * @description
 */
@Data
public class DoneItemVo {

    private Long itemId;

    private Integer status;

    private String reason;
}
