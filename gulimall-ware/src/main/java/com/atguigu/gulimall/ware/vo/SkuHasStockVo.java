package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @author cyl
 * @date 2021/11/27/22:16
 * @description
 */
@Data
public class SkuHasStockVo {

    private Long skuId;

    private Boolean hasStock;
}
